## How to instantiate

- First create a database named ```nawshad_fb```
- Then import the sql file ```nawshad_fb.sql``` (which is included in the repository) to database
- Then run ```composer update``` command
- Rename the ```.env.example``` file to ```.env```, then set the ```FB_APP_ID``` and ```FB_APP_SECRET```  parameters
- Also update the database parameters accordingly
- Create a virtualhost named ```naushadfb.com``` and set it to access ```project_root/web``` directory
- Then go to ```naushadfb.com``` and it will run. Hopefully.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)