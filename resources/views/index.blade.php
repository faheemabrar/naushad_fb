<html>
<head>
    <title>FB</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    @if(isset($login_url))
                        <a class="btn btn-info" href="{{ $login_url }}">Click here to login</a>
                    @else
                        <a class="btn btn-danger" href="{{ route('logout') }}">Click here to logout</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</header>
<div class="container">
    @if(Session::has('fb_access_token'))
        <div class="row">
            <div class="col-md-12">
                <form action="{{route('post_post')}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="control-label" for="message">Create Post</label>
                        <input type="text" class="form-control" name="message"/>
                    </div>
                    <input type="submit" class="btn btn-success" value="Submit"/>
                </form>
            </div>
        </div>
        @if(isset($posts))
            <div class="row">
                @foreach($posts as $post)
                    <div class="panel panel-success">
                        <div class="panel-heading">{{$post['message']}}</div>
                        <div class="panel-body">
                            @foreach($post['comments'] as $comment)
                                <div class="well well-sm"><span class="label label-primary">{{$comment['from']['name']}}</span> {{$comment['message']}}</div>
                            @endforeach
                            <div class="well well-sm">
                                <form action="{{route('post_comment')}}" method="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input name="post_id" value="{{$post['id']}}" type="hidden"/>
                                    <div class="form-group">
                                        <label for="message">Post a comment</label>
                                        <input type="text" class="form-control" name="message"/>
                                    </div>
                                    <input type="submit" class="btn btn-success" value="Submit"/>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <p class="text-center">No posts</p>
        @endif
    @endif
</div>
</body>
</html>