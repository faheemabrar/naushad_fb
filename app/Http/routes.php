<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as'=>'index','uses'=>'MainController@index']);
Route::get('/login_success', ['as'=>'login_success','uses'=>'MainController@login_success']);
Route::post('/post_post',['as'=>'post_post','uses'=>'MainController@post_post']);
Route::post('/post_comment',['as'=>'post_comment','uses'=>'MainController@post_comment']);
Route::get('/logout',['as'=>'logout','uses'=>'MainController@logout']);
