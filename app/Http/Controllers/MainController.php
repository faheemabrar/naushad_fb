<?php

namespace App\Http\Controllers;

use Facebook\Facebook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class MainController extends Controller{

    private $fb;

    public function __construct()
    {
        session_start();
        $this->fb = new Facebook([
            'app_id' => env('FB_APP_ID'),
            'app_secret' => env('FB_APP_SECRET'),
            'default_graph_version' => 'v2.4',
        ]);
    }

    public function index(Request $request)
    {
        //var_dump([session('fb_id'),session('fb_access_token')]);
        //first, check if session already exists
        //if session exists, pull posts and comments
        //else, login and redirect
        if($request->session()->has('fb_access_token')){
            $token = session('fb_access_token');
            $posts = app('db')->select(" select post_id from post join user on user.id = post.user_id and user.token = '$token' ");
            $data = $this->get_posts($posts);
            return view('index',$data);
        }
        else{
            $helper = $this->fb->getRedirectLoginHelper();
            $permissions = ['publish_pages','manage_pages']; // optional
            $data['login_url'] = $helper->getLoginUrl(route('login_success'), $permissions);
            return view('index',$data);
        }
    }

    public function login_success()
    {
        $helper = $this->fb->getRedirectLoginHelper();
        try {
            $accessToken = $helper->getAccessToken();
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (isset($accessToken)) {
            // Logged in!
            $oAuth2Client = $this->fb->getOAuth2Client();
            $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            $page_id = env('FB_PAGE_ID');
            try {
                // Returns a `Facebook\FacebookResponse` object
                $response = $this->fb->get('/me?fields=id', $longLivedAccessToken);
                $page = $this->fb->get("/$page_id?fields=access_token", $longLivedAccessToken);
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            $user = $response->getGraphUser();
            $id = $user['id'];
            $longLivedAccessToken = $page->getGraphNode()['access_token'];
            session(['fb_access_token' => (string) $longLivedAccessToken]);
            $this->fb->setDefaultAccessToken($longLivedAccessToken);
            session(['fb_id' => (string) $id]);
            if(count(app('db')->select(" select * from user where fb_id = '$id' ")) == 0){
                app('db')->insert('insert into user (fb_id, token) values (?, ?)', [session('fb_id'), session('fb_access_token')]);
            }
            else{
                app('db')->update('update user set token = ? where fb_id = ?',[session('fb_access_token'),session('fb_id')]);
            }
            return redirect()->to(route('index'));
        }
    }

    private function get_posts($posts)
    {
        $data = [];
        foreach($posts as $post)
        {
            try {
                // Returns a `Facebook\FacebookResponse` object
                $response = $this->fb->get("/{$post->post_id}",session('fb_access_token'));
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                app('db')->delete(" delete from post where post_id = ? ",[$post->post_id]);
                continue;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            $temp_post = $response->getGraphNode();
            $post_id = $temp_post['id'];

            try {
                // Returns a `Facebook\FacebookResponse` object
                $response = $this->fb->get("/$post_id/comments",session('fb_access_token'));
                $comment_nodes = $response->getGraphEdge();
                $temp_post['comments'] = $comment_nodes;
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                continue;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            $data['posts'][] = $temp_post;
        }
        return $data;
    }

    public function post_post(Request $request)
    {
        $page_id = env('FB_PAGE_ID');
        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $this->fb->post("/$page_id/feed",['message'=>$request->get('message')],session('fb_access_token'));
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $graphNode = $response->getGraphNode();
        $id = $graphNode['id'];
        $user = app('db')->select(" select id from user where fb_id = ? and token = ? ",[session('fb_id'),session('fb_access_token')]);
        $user_id = $user[0]->id;
        app('db')->insert(" insert into post values (NULL,?,?) ",[$id,$user_id]);
        return redirect(route('index'));
    }

    public function post_comment(Request $request)
    {
        $post_id = $request->get('post_id');
        $message = $request->get('message');
        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $this->fb->post("/$post_id/comments",['message'=>$message],session('fb_access_token'));
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $graphNode = $response->getGraphNode();
        return redirect()->to(route('index'));
    }

    public function logout()
    {
        if(\Session::has('fb_access_token')){
            \Session::flush();
        }
        return redirect(route('index'));
    }
}